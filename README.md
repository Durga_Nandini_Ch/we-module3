# WE-Module3



## Generative AI course work

This is the coursework repository for the Generative AI classes offered as part of Module 3 in the Women Engineers program by Talentsprint, supported by Google. This repository contains the coursework materials and code for the classes completed during the course.

## Tasks completed :

- Solved a computational thinking problem using Generative AI
- Understood what the game of Yatzee is and engineered the genAI tool to play the game with me
- Wrote code and created a testing strategy for the Yatzee code using genAI
- Implemented Markov chain model for text generation
- Developed strategies for the bidding card game 'Diamonds' using GenAI
