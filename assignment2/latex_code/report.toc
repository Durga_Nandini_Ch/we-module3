\contentsline {section}{\numberline {0.1}Introduction}{2}{section.0.1}%
\contentsline {section}{\numberline {0.2}Problem Statement}{2}{section.0.2}%
\contentsline {section}{\numberline {0.3}Teaching GenAI the Game}{2}{section.0.3}%
\contentsline {subsection}{\numberline {0.3.1}Rule Comprehension}{2}{subsection.0.3.1}%
\contentsline {subsection}{\numberline {0.3.2}Sample Gameplay}{3}{subsection.0.3.2}%
\contentsline {section}{\numberline {0.4}Iterating upon the Strategy}{3}{section.0.4}%
\contentsline {subsection}{\numberline {0.4.1}Strategy Analysis}{3}{subsection.0.4.1}%
\contentsline {subsection}{\numberline {0.4.2}Feedback and Improvement Suggestions}{3}{subsection.0.4.2}%
\contentsline {section}{\numberline {0.5}Code Development and Refinement}{4}{section.0.5}%
\contentsline {section}{\numberline {0.6}Conclusion of the Study}{4}{section.0.6}%
\contentsline {section}{\numberline {0.7}Additional Resources}{5}{section.0.7}%
